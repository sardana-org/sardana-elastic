# How to setup Sardana Elastic Stack integration in K8s cluster

The main components of this setup are:
- Sardana server (write logs into a file)
- Filebeat agent (fetch logs from the file and send them to Logstash)
- Logstash (process log lines and send structured data into Elasticsearch)
- Elasticsearch (keep the data)
- Kibana (visualize the data)
  
## Prerequisites

1. Sardana installed (see this [guide](https://sardana-controls.org/users/getting_started/installing.html)
   on how to install Sardana)
2. Minikube installed (see this [guide](https://minikube.sigs.k8s.io/docs/start/) on how to install Minikube) and running.

   
## K8s cluster setup

[ECK](https://www.elastic.co/guide/en/cloud-on-k8s/current/index.html)
extends the basic Kubernetes orchestration capabilities to support the 
setup and management of Elasticsearch, Kibana, etc.


1. Follow this [guide](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-deploy-eck.html)
    in order to instantiate the *elastic operator* and the rest of the 
    resources. You can still use the following steps but keep in mind 
    that at some point these may get outdated (at the time of writing of
    this guide the ECK was at version 2.0).

    ```
    kubectl create -f https://download.elastic.co/downloads/eck/2.0.0/crds.yaml
    kubectl apply -f https://download.elastic.co/downloads/eck/2.0.0/operator.yaml
    ```

2. Wait approx. 10 s

3. Create Elasticsearch, Logstash and Kibana resources:
    ```
    kubectl create namespace sardana-elastic;
    kubectl -n sardana-elastic create \
        -f elasticsearch.yaml \
        -f kibana.yaml \
        -f logstash_configmap.yaml \
        -f logstash_pod.yaml \
        -f logstash_service.yaml
    ```

4. Wait approx 6 min until the resources are ready (all green and running):
    ```
    watch kubectl -n sardana-elastic get elasticsearch,kibana,pod
    ```
5. Publish Logstash and Kibana ports on localhost:
    ```
    kubectl -n sardana-elastic port-forward service/quickstart-kb-http 5601
    kubectl -n sardana-elastic port-forward service/logstash 5044
    ```
    
## Local setup

### Setup Filebeat

1. Follow this [guide](https://www.elastic.co/downloads/beats/filebeat)
   in order to install Filebeat.
2. Configure Filebeat (in `/etc/filebeat/filebeat.yml`):
    - configure input:
        ```
        filebeat.inputs:
        - type: log
          paths:
            - /tmp/tango-zreszela/Sardana/zreszela/log.txt
        ```
    - configure output:
        ```
        output.logstash:
          hosts: ["localhost:5044"]
        ```
3. Start Sardana server

## Browse logs with Kibana

1. Login to Kibana (https://localhost:5601) using the *elastic* user and the following password:
    ```
    kubectl -n sardana-elastic get secret quickstart-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo

2. Create Index Pattern for Sardana logs:
   - Management
   - Stack Management
   - Kibana
   - Index Pattern
   - Create Index Pattern
   - Name: logstash-filebeat
   - Timestamp field: @timestamp

3. Browse Sardana logs documents
   - Analytics
   - Disscover